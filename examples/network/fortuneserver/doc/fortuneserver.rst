Fortune Server Example
======================

Demonstrates how to create a server for a network service. It corresponds to the
Qt example `Fortune Server Example
<https://doc.qt.io/qt-6/qtnetwork-fortuneserver-example.html>`_.

.. image:: fortuneserver.png
    :align: center
    :alt: fortuneserver screenshot
    :width: 400
