D-Bus List Names Example
========================

Shows how to access the D-Bus bus daemon service.

List Names is a command-line example which shows how to access the Qt D-Bus bus
daemon service. The example prints various information about the bus daemon service
