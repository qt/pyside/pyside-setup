WebEngine Markdown Editor Example
=================================

Demonstrates how to integrate a web engine in a hybrid desktop application.
It corresponds to the Qt example `WebEngine Markdown Editor Example
<https://doc.qt.io/qt-5/qtwebengine-webenginewidgets-markdowneditor-example.html>`_.

.. image:: markdowneditor.png
    :align: center
    :alt: markdowneditor screenshot
    :width: 400
