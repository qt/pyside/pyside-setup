StateMachine Ping Pong Example
==============================

The Ping Pong States example shows how to use parallel states together
with custom events and transitions in The State Machine Framework.
It corresponds to the Qt example `Ping Pong States Example
<https://doc.qt.io/qt-5/qtwidgets-statemachine-pingpong-example.html>`_.

.. image:: pingpong.png
    :align: center
    :alt: ping_pong screenshot
    :width: 400
