Colliding Mice Example
======================

This application, is a graphical simulation built using PySide6. It creates a
scene with multiple animated mouse objects that move around and interact with
each other within a `QGraphicsView`. The mice change direction to avoid
collisions and exhibit random movements, demonstrating the use of
`QGraphicsItem`, `QGraphicsScene`, and `QGraphicsView` for creating dynamic and
interactive graphics in a PySide6 application. This example demonstrates the
analogous Qt example `Colliding Mice Example
<https://doc.qt.io/qt-6/qtwidgets-graphicsview-collidingmice-example.html>`_.

.. image:: collidingmice.webp
    :width: 400
    :alt: collidingmice screenshot
