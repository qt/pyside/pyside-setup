Anchor Layout Example
=====================

This application demonstrates the use of `QGraphicsAnchorLayout` in a PySide6 Widget application.
It creates a graphical scene with several `QPushButton` widgets arranged using anchor constraints,
allowing for dynamic and flexible layout management within a `QGraphicsView`. It demonstrates
the analogous Qt example
`Anchor Layout Example <https://doc.qt.io/qt-6.2/qtwidgets-graphicsview-anchorlayout-example.html>`_

.. image:: anchorlayout.webp
    :width: 400
    :alt: anchorlayout screenshot
