Trivial Wizard Example
======================

The Trivial Wizard example illustrates how to create a linear three-page
registration wizard using three instances of `QWizardPage` and one instance of
`QWizard`. This example demonstrates the analogous Qt example `Trivial Wizard
Example <https://doc.qt.io/qt-6/qtwidgets-dialogs-trivialwizard-example.html>`_.

.. image:: trivialwizard.png
    :width: 400
    :alt: trivialwizard screenshot
