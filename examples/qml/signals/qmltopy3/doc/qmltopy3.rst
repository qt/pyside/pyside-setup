Handling QML Signals in Python
==============================

Show how to respond to signals emitted from QML in Python.

**Key Features:**

- **Defining Signals in QML:** Defines custom signals like `buttonClicked` and `textRotationChanged`
- **Connecting Signals to Python Functions:** In Python, connects these signals to functions.
- **Handling Signals in Python:** Implements the `sayThis` function to handle the signals.
